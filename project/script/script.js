//1. объектная модель документа, которую браузер создает в памяти компьютера на основании HTML-кода, полученного им от сервера
//2. если внутри innerText будутэлементы HTML со своим содержимым, то он проигнорирует сами элементы и вернёт их внутренний текст, innerHTML — покажет текстовую информацию ровно по одному элементу
//3. getElementById () getElementsByName () getElementsByTagName () getElementsByClassName () querySelectorAll () querySelector (). думаю querySelectorAll () лучший



const allP = document.querySelectorAll('p')
console.log(allP);
allP.forEach((p) => {
    p.style.backgroundColor = '#ff0000';
})

let optionList = document.querySelector('#optionsList')
console.log(optionsList);
let parentOptionList = optionsList.parentElement
console.log(parentOptionList);
let optionsListNodes = [...optionsList.childNodes]
console.log(optionsListNodes);
optionsListNodes.forEach((item) => {
    console.log(item);
})

let testP = document.querySelector('#testParagraph');
testP.textContent = 'This is a paragraph';

let mainH = document.querySelector('.main-header');
let arrayElements = [...mainH.children];
arrayElements.forEach((element) => {
    console.log(element);
    element.classList.add('nav-item');
})

let allItems = document.querySelectorAll('.section-title')
allItems.forEach((item) => {
    item.classList.remove('section-title');
    console.log(item.classList);
})





